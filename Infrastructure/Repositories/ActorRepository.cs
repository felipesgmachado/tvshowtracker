﻿using Domain.Actors;
using Domain.Episodes;
using Domain.Shows;
using Domain.Users;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ActorRepository : IActorRepository
    {
        private readonly AppDbContext _dbContext;

        public ActorRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Actor> GetByNameAsync(string name)
        {
            return await _dbContext.Actors.FirstOrDefaultAsync(x => x.Name == name);
        }
        
        public async Task<Actor> GetShowsByActorAsync(string name)
        {
            return await _dbContext.Actors.Include(e => e.Shows).FirstOrDefaultAsync(x => x.Name == name);
        }

        public async Task InsertAsync(Actor actor)
        {
            _dbContext.Actors.Add(actor);
        }

        public async Task UpdateAsync(Actor actor)
        {
            _dbContext.Update(actor);
            await _dbContext.SaveChangesAsync();
        }        
    }
}
