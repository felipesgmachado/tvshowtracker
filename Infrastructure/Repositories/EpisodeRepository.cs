﻿using Domain.Episodes;
using Infrastructure.Context;

namespace Infrastructure.Repositories;

public class EpisodeRepository : IEpisodeRepository
{
    private readonly AppDbContext _dbContext;

    public EpisodeRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task InsertAsync(Episode episode)
    {
        _dbContext.Episodes.Add(episode);
    }
}
