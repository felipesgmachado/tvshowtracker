﻿using Domain.Seasons;
using Infrastructure.Context;

namespace Infrastructure.Repositories;

public class SeasonRepository : ISeasonRepository
{
    private readonly AppDbContext _dbContext;

    public SeasonRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task InsertAsync(Season season)
    {
        _dbContext.Seasons.Add(season);
    }
}
