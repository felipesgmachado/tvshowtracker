﻿using Domain.Favorites;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class FavoriteRepository : IFavoriteRepository
    {
        private readonly AppDbContext _dbContext;

        public FavoriteRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task DeleteAsync(Favorite favorite)
        {
            _dbContext.Remove(favorite);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Favorite?> GetByIdAsync(Guid id)
        {
            return await _dbContext.Favorites.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Favorite?> GetByShowAsync(Guid showId)
        {
            return await _dbContext.Favorites.FirstOrDefaultAsync(x => x.ShowId == showId);
        }

        public async Task<IEnumerable<Favorite?>> GetByUserIdAsync(Guid userId)
        {
            return await _dbContext.Favorites.Where(x => x.UserId == userId).AsNoTracking().ToListAsync(); ;
        }

        public async Task InsertAsync(Favorite favorite)
        {
            _dbContext.Favorites.Add(favorite);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Favorite favorite)
        {
            _dbContext.Update(favorite);
            await _dbContext.SaveChangesAsync();
        }
    }
}
