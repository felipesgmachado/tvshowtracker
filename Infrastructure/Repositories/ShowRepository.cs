﻿using Domain.Shows;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using Type = Domain.Shows.Type;

namespace Infrastructure.Repositories;

public class ShowRepository : IShowRepository
{
    private readonly AppDbContext _dbContext;

    public ShowRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<(int, IEnumerable<Show>)> GetAllShowsAsync(int pageSize, int pageIndex, string? orderBy = null)
    {
        var total = await _dbContext.Shows.AsNoTracking().CountAsync();
        var result = _dbContext.Shows
            .Include(shows => shows.Actors)
            .Include(shows => shows.Seasons)
            .ThenInclude(seasons => seasons.Episode)
            .AsQueryable();

        if (orderBy?.ToLower() == "name")
            result = result.OrderBy(x => x.Name);
        else if (orderBy?.ToLower() == "releasedate")
            result = result.OrderBy(x => x.ReleaseDate);
        else if (orderBy?.ToLower() == "details")
            result = result.OrderBy(x => x.Details);
        else if (orderBy?.ToLower() == "genre")
            result = result.OrderBy(x => x.Genre);
        else if (orderBy?.ToLower() == "type")
            result = result.OrderBy(x => x.Type);

        var resultList = await result.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();

        return (total, resultList);
    }

    public async Task<Show> GetActorsByShowAsync(string name)
    {
        return _dbContext.Shows.Include(shows => shows.Actors.Where(actor => actor.Name == name)).FirstOrDefault();        
    }

    public async Task<Show> GetByNameAsync(string name)
    {
        var result = await  _dbContext.Shows
            .Where(show => show.Name == name)
            .Include(Show => Show.Actors)
            .Include(show => show.Seasons)
            .ThenInclude(season => season.Episode)
            .FirstOrDefaultAsync();

        return result;
    }

    public async Task<(int, IEnumerable<Show>)> GetByGenreAsync(Genre genre, int pageSize, int pageIndex, string? orderBy = null)
    {
        var total = await _dbContext.Shows.Where(show => show.Genre == genre)
            .Include(shows => shows.Actors)
            .Include(shows => shows.Seasons)
            .ThenInclude(seasons => seasons.Episode)
            .AsNoTracking().CountAsync();

        var result = _dbContext.Shows.Where(show => show.Genre == genre)
            .Include(shows => shows.Actors)
            .Include(shows => shows.Seasons)
            .ThenInclude(seasons => seasons.Episode)
            .AsQueryable();

        if (orderBy?.ToLower() == "name")
            result = result.OrderBy(x => x.Name);
        else if (orderBy?.ToLower() == "releasedate")
            result = result.OrderBy(x => x.ReleaseDate);
        else if (orderBy?.ToLower() == "details")
            result = result.OrderBy(x => x.Details);
        else if (orderBy?.ToLower() == "genre")
            result = result.OrderBy(x => x.Genre);
        else if (orderBy?.ToLower() == "type")
            result = result.OrderBy(x => x.Type);

        var resultList = await result.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();

        return (total, resultList);
    }

    public async Task<Show> GetByIdAsync(Guid id)
    {
        var result = await _dbContext.Shows
            .Where(show => show.Id == id)
            .Include(show => show.Actors)
            .Include(show => show.Seasons)
            .ThenInclude(season => season.Episode)
            .FirstOrDefaultAsync();

        return result;
    }

    public async Task<(int, IEnumerable<Show>)> GetByTypeAsync(Type type, int pageSize, int pageIndex, string? orderBy = null)
    {
        var total = await _dbContext.Shows.Where(show => show.Type == type)
            .Include(shows => shows.Actors)
            .Include(shows => shows.Seasons)
            .ThenInclude(seasons => seasons.Episode)
            .AsNoTracking().CountAsync();

        var result = _dbContext.Shows.Where(show => show.Type == type)
            .Include(shows => shows.Actors)
            .Include(shows => shows.Seasons)
            .ThenInclude(seasons => seasons.Episode)
            .AsQueryable();

        if (orderBy?.ToLower() == "name")
            result = result.OrderBy(x => x.Name);
        else if (orderBy?.ToLower() == "releasedate")
            result = result.OrderBy(x => x.ReleaseDate);
        else if (orderBy?.ToLower() == "details")
            result = result.OrderBy(x => x.Details);
        else if (orderBy?.ToLower() == "genre")
            result = result.OrderBy(x => x.Genre);
        else if (orderBy?.ToLower() == "type")
            result = result.OrderBy(x => x.Type);

        var resultList = await result.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();

        return (total, resultList);
    }

    public async Task InsertAsync(Show show)
    {
        _dbContext.Shows.Add(show);
    }

    public async Task UpdateAsync(Show show)
    {
        _dbContext.Shows.Update(show);        
    }

    public async Task<IEnumerable<Show>> GetWithNameAsync(string name)
    {
        var shows =  _dbContext.Shows.Where(x => x.Name.Contains(name))
            .Include(shows => shows.Actors)
            .Include(shows => shows.Seasons)
            .ThenInclude(seasons => seasons.Episode)            
            .AsNoTracking()
            .AsEnumerable();            

        return shows.ToList();
    }

    public async Task<IEnumerable<Show>> GetByGenreAsync(Genre genre)
    {
        var result = _dbContext.Shows.Where(show => show.Genre == genre)
            .Include(shows => shows.Actors)
            .Include(shows => shows.Seasons)
            .ThenInclude(seasons => seasons.Episode)
            .AsNoTracking()
            .AsEnumerable();

        return result.ToList();
    }

    public async Task<IEnumerable<Show>> GetShowsWithActor(string name)
    {
        var result = _dbContext.Shows
            .Include(show => show.Actors.Where(actor => actor.Name.Contains(name))
            .AsQueryable())
            .AsNoTracking();            

        return result.ToList();
    }
}
