﻿namespace Infrastructure.Services.Email;

public interface IEmailService
{
    Task<Response> SendEmailAsync(Message message);
}
