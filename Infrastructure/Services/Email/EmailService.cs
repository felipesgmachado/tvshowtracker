﻿using Domain.Shows;
using Domain.Users;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace Infrastructure.Services.Email;

public class EmailService : IEmailService
{
    private readonly EmailConfiguration _emailConfig;
    public EmailService(EmailConfiguration emailConfig)
    {
        _emailConfig = emailConfig;
    }

    //public string? User { get; set; }
    public async Task<Response> SendEmailAsync(Message message)
    {
        var emailMessage = CreateEmailMessage(message);
        await SendAsync(emailMessage);
        return new Response(message.To, "Sent");
    }

    private async Task SendAsync(MimeMessage mailMessage)
    {
        using (var client = new SmtpClient())
        {
            try
            {
                await client.ConnectAsync(_emailConfig.SmtpServer, _emailConfig.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(_emailConfig.UserName, _emailConfig.Password);
                await client.SendAsync(mailMessage);
            }
            catch
            {
                //TODO: log an error message or throw an exception, or both.
                throw;
            }
            finally
            {
                await client.DisconnectAsync(true);
                client.Dispose();
            }
        }
    }

    private MimeMessage CreateEmailMessage(Message message)
    {
        var emailMessage = new MimeMessage();
        emailMessage.From.Add(new MailboxAddress("TV Show Tracker", _emailConfig.From));
        emailMessage.To.AddRange(message.To.Select(x => new MailboxAddress(x.Name, x.Address)));
        emailMessage.Subject = message.Subject;
        emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = message.Content };
        return emailMessage;
    }
}
