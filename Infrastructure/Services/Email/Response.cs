﻿using Infrastructure.Services.Shared;

namespace Infrastructure.Services.Email;

public class Response : BaseResponse
{
    public Response()
    {
            
    }
    public Response(List<EmailAddress> to, string message)
    {
        IsError = false;
        To = new List<EmailAddress>(to);
        Message = message; 
    }

    public string Message { get; set; }
    public List<EmailAddress> To { get; set; }
}
