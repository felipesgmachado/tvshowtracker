﻿using Domain.Shows;
using System.Text.Json;

namespace Infrastructure.Services.Email;
public class Message
{
    public Message(List<EmailAddress> to, string subject, string content)
    {
        To = new List<EmailAddress>(to);
        Subject = subject;
        Content = content;
    }

    public List<EmailAddress> To { get; set; }
    public string Subject { get; set; }
    public string Content { get; set; }        
}

public class EmailAddress
{
    public string Name { get; set; }
    public string Address { get; set; }
}
