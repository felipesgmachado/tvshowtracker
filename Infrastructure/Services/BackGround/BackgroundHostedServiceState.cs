﻿namespace Infrastructure.Services.BackGround;
public record BackgroundHostedServiceState(bool IsEnabled);