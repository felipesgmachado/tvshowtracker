﻿using Infrastructure.Context;

namespace Infrastructure.Services.Transations;

public class UnitOfWork : IUnitOfWork
{
    private readonly AppDbContext _context;

    public UnitOfWork(AppDbContext context)
    {
        _context = context;
    }

    public async Task Commit()
    {
        await _context.SaveChangesAsync();
    }

    public async Task Rollback()
    {
        await Task.CompletedTask;
    }
}