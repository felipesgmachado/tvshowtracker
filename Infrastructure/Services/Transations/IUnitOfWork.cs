﻿namespace Infrastructure.Services.Transations;

public interface IUnitOfWork
{
    Task Commit();
    Task Rollback();
}
