﻿namespace Infrastructure.Services.Auth;

public interface IAuthService
{
    Task<bool> RegisterUserAsync(string email, string password);
    Task<string> LoginUserAsync(string email, string password);
}
