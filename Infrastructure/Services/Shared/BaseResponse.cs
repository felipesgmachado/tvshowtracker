﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Infrastructure.Services.Shared
{
    public class BaseResponse
    {
        [JsonIgnore]
        public bool IsError { get; set; } = true;
    }
}
