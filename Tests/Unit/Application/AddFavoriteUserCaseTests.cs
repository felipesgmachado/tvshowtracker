﻿using ApplicationCore.Favorites.AddFavorite;
using Domain.Favorites;
using Domain.Shows;
using Domain.Users;
using Moq;
using Type = Domain.Shows.Type;
using FluentAssertions;

namespace Tests.Unit.Application;

public class AddFavoriteUserCaseTests
{
    private readonly Mock<IShowRepository> _showRepositoryMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<IFavoriteRepository> _favoriteRepositoryMock;

    public AddFavoriteUserCaseTests()
    {
        _showRepositoryMock = new();
        _userRepositoryMock = new();
        _favoriteRepositoryMock = new();
    }

    [Fact]
    public async Task Handle_WithValidInputAndExistingUserAndShow_ShouldReturnAddFavoriteOutput()
    {
        // Arrange
        var input = new AddFavoriteInput { ShowName = "Test Show" };
        string userEmail = "testuser@example.com";
        var user = new User("testuser", userEmail, true);
        var show = new Show(input.ShowName, "Show details", DateTime.Now, Genre.Animation, Type.Movies);
        var favorite = new Favorite(show.Id, user.Id);
        var expectedOutput = new AddFavoriteOutput(favorite);

        _userRepositoryMock.Setup(repo => repo.GetByEmailAsync(userEmail))
                          .ReturnsAsync(user);
        _showRepositoryMock.Setup(repo => repo.GetByNameAsync(input.ShowName))
                          .ReturnsAsync(show);
        _favoriteRepositoryMock.Setup(repo => repo.GetByShowAsync(show.Id))
                             .ReturnsAsync(favorite);

        var useCase = new AddFavoriteUserCase(_showRepositoryMock.Object, _userRepositoryMock.Object, _favoriteRepositoryMock.Object);

        // Act
        var resultOutput = await useCase.Handle(input, userEmail);

        // Assert
        Assert.NotNull(resultOutput);        
        Assert.Equivalent(expectedOutput.IsError, resultOutput.IsError);
    }
}
