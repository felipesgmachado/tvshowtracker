﻿using ApplicationCore.Actors;
using Domain.Actors;
using Moq;

namespace Tests.Unit.Application
{
    public class GetShowsByActorNameUseCaseTests
    {
        private readonly Mock<IActorRepository> _actorRepositoryMock;

        public GetShowsByActorNameUseCaseTests()
        {
            _actorRepositoryMock = new();                
        }

        [Fact]
        public async Task Handle_WithValidInput_ShouldReturnGetShowsByActorNameOutput()
        {
            // Arrange
            string actorName = "John Doe";
            var actor = new Actor(actorName);
            var expectedActor = new GetShowsByActorNameOutput(actor);

            _actorRepositoryMock.Setup(x => x.GetShowsByActorAsync(actorName)).ReturnsAsync(actor);

            var useCase = new GetShowsByActorNameUseCase(_actorRepositoryMock.Object);            

            // Act
            var result = await useCase.Handle(actorName);

            // Assert
            Assert.NotNull(result);
            Assert.Equivalent(expectedActor, result);
        }

        [Fact]
        public async Task Handle_WithNullActor_ShouldReturnEmptyGetShowsByActorNameOutput()
        {
            // Arrange
            string actorName = "John Doe";
            var actor = new Actor(actorName);
            var expectedOutput = new GetShowsByActorNameOutput();

            _actorRepositoryMock.Setup(x => x.GetShowsByActorAsync(It.IsAny<string>())).ReturnsAsync(It.IsAny<Actor>());
            var useCase = new GetShowsByActorNameUseCase(_actorRepositoryMock.Object);            

            // Act
            var resultOutput = await useCase.Handle(actorName);

            // Assert
            Assert.NotNull(resultOutput);
            Assert.Equivalent(expectedOutput, resultOutput);
        }
    }
}
