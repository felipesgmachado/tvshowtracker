﻿using Domain.Seasons;

namespace Tests.Unit.Domain;

public class SeasonTests
{
    [Fact]
    public void SeasonConstructor_WithValidArguments_ShouldCreateSeasonObject()
    {
        // Arrange
        Guid showId = Guid.NewGuid();
        string name = "Test Season";
        string details = "Test details";
        DateTime releaseDate = DateTime.Now;

        // Act
        Season season = new Season(showId, name, details, releaseDate);

        // Assert
        Assert.NotNull(season);
        Assert.Equal(name, season.Name);
        Assert.Equal(details, season.Details);
        Assert.Equal(releaseDate, season.ReleaseDate);
        Assert.Equal(showId, season.ShowId);
        Assert.NotNull(season.Episode);
    }

    [Theory]
    [InlineData("", "Test details", "2023-01-01", "7c826db4-64e5-42a6-8c72-72b1b8c6cd9e")]
    [InlineData("Test Season", "", "2023-01-01", "7c826db4-64e5-42a6-8c72-72b1b8c6cd9e")]
    public void SeasonConstructor_WithInvalidArguments_ShouldThrowArgumentException(
        string name, string details, string releaseDateString, string showIdString)
    {
        // Arrange
        DateTime releaseDate = DateTime.Parse(releaseDateString);
        Guid showId = Guid.Parse(showIdString);

        // Act & Assert
        Assert.Throws<ArgumentException>(() => new Season(showId, name, details, releaseDate));
    }

    [Fact]
    public void SeasonConstructor_WithDefaultReleaseDate_ShouldThrowArgumentNullException()
    {
        // Arrange
        Guid showId = Guid.NewGuid();
        string name = "Test Season";
        string details = "Test details";
        DateTime releaseDate = default;

        // Act & Assert
        Assert.Throws<ArgumentNullException>(() => new Season(showId, name, details, releaseDate));
    }
}
