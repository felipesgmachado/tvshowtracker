﻿using Domain.Favorites;

namespace Tests.Unit.Domain;

public class FavoriteTests
{
    [Fact]
    public void FavoriteConstructor_WithValidArguments_ShouldCreateFavoriteObject()
    {
        // Arrange
        Guid showId = Guid.NewGuid();
        Guid userId = Guid.NewGuid();

        // Act
        Favorite favorite = new Favorite(showId, userId);

        // Assert
        Assert.NotNull(favorite);
        Assert.NotEqual(Guid.Empty, favorite.Id);
        Assert.Equal(showId, favorite.ShowId);
        Assert.Equal(userId, favorite.UserId);
    }
}
