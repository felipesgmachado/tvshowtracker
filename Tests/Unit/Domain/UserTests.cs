﻿using Domain.Favorites;
using Domain.Users;

namespace Tests.Unit.Domain;

public class UserTests
{
    [Fact]
    public void UserConstructor_WithValidArguments_ShouldCreateUserObject()
    {
        // Arrange
        string name = "John Doe";
        string email = "john.doe@example.com";
        bool isRGPDCompliance = true;

        // Act
        User user = new User(name, email, isRGPDCompliance);

        // Assert
        Assert.NotNull(user);
        Assert.NotEqual(Guid.Empty, user.Id);
        Assert.Equal(name, user.Name);
        Assert.Equal(email, user.Email);
        Assert.True(user.IsRGPDCompliance);
    }

    [Theory]
    [InlineData("", "john.doe@example.com", true)]
    [InlineData("John Doe", "", true)]
    public void UserConstructor_WithInvalidArguments_ShouldThrowArgumentException(
        string name, string email, bool isRGPDCompliance)
    {
        // Act & Assert
        Assert.Throws<ArgumentException>(() => new User(name, email, isRGPDCompliance));
    }
}