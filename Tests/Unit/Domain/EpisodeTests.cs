﻿using Domain.Episodes;

namespace Tests.Unit.Domain;

public class EpisodeTests
{
    [Fact]
    public void EpisodeConstructor_WithValidArguments_ShouldCreateEpisodeObject()
    {
        // Arrange
        Guid seasonId = Guid.NewGuid();
        int number = 1;
        string title = "Test Episode";
        string details = "Test details";
        DateTime releaseDate = DateTime.Now;
        int duration = 30;

        // Act
        Episode episode = new Episode(seasonId, number, title, details, releaseDate, duration);

        // Assert
        Assert.NotNull(episode);
        Assert.NotEqual(Guid.Empty, episode.Id);
        Assert.Equal(seasonId, episode.SeasonId);
        Assert.Equal(number, episode.Number);
        Assert.Equal(title, episode.Title);
        Assert.Equal(details, episode.Details);
        Assert.Equal(releaseDate, episode.ReleaseDate);
        Assert.Equal(duration, episode.Duration);
    }

    [Theory]
    [InlineData("", "Test details", "2023-01-01", 30, 1, "7c826db4-64e5-42a6-8c72-72b1b8c6cd9e")]
    [InlineData("Test Episode", "", "2023-01-01", 30, 1, "7c826db4-64e5-42a6-8c72-72b1b8c6cd9e")]
    public void EpisodeConstructor_WithInvalidArguments_ShouldThrowArgumentException(
        string title, string details, string releaseDateString, int duration, int number, string seasonIdString)
    {
        // Arrange
        DateTime releaseDate = DateTime.Parse(releaseDateString);
        Guid seasonId = Guid.Parse(seasonIdString);

        // Act & Assert
        Assert.Throws<ArgumentException>(() => new Episode(seasonId, number, title, details, releaseDate, duration));
    }

    [Fact]
    public void EpisodeConstructor_WithDefaultReleaseDate_ShouldThrowArgumentNullException()
    {
        // Arrange
        Guid seasonId = Guid.NewGuid();
        int number = 1;
        string title = "Test Episode";
        string details = "Test details";
        int duration = 30;

        // Act & Assert
        Assert.Throws<ArgumentNullException>(() => new Episode(seasonId, number, title, details, default, duration));
    }

    [Fact]
    public void EpisodeConstructor_WithEmptySeasonId_ShouldThrowArgumentNullException()
    {
        // Arrange
        int number = 1;
        string title = "Test Episode";
        string details = "Test details";
        DateTime releaseDate = DateTime.Now;
        int duration = 30;

        // Act & Assert
        Assert.Throws<ArgumentNullException>(() => new Episode(Guid.Empty, number, title, details, releaseDate, duration));
    }  
}
