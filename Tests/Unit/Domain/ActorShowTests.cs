﻿using Domain.Actors;
using Domain.Shows;
using Type = Domain.Shows.Type;

namespace Tests.Unit.Domain;

public class ActorShowTests
{
    [Fact]
    public void ActorShowProperties_ShouldBeSetCorrectly()
    {
        // Arrange
        Show show = new Show("Test Show", "Test details", DateTime.Now, Genre.Action, Type.Movies);
        Actor actor = new Actor("John Doe");
        Guid showId = Guid.NewGuid();
        Guid actorId = Guid.NewGuid();

        // Act
        ActorShow actorShow = new ActorShow
        {
            Show = show,
            ShowsId = showId,
            Actor = actor,
            ActorsId = actorId
        };

        // Assert
        Assert.NotNull(actorShow);
        Assert.Equal(show, actorShow.Show);
        Assert.Equal(showId, actorShow.ShowsId);
        Assert.Equal(actor, actorShow.Actor);
        Assert.Equal(actorId, actorShow.ActorsId);
    }

    [Fact]
    public void ActorShowProperties_CanBeModified()
    {
        // Arrange
        Show show1 = new Show("Test Show 1", "Test details", DateTime.Now, Genre.Action, Type.Movies);
        Show show2 = new Show("Test Show 2", "Test details", DateTime.Now, Genre.Comedy, Type.Series);
        Actor actor1 = new Actor("John Doe");
        Actor actor2 = new Actor("Jane Doe");
        Guid showId1 = Guid.NewGuid();
        Guid showId2 = Guid.NewGuid();
        Guid actorId1 = Guid.NewGuid();
        Guid actorId2 = Guid.NewGuid();

        // Act
        ActorShow actorShow = new ActorShow
        {
            Show = show1,
            ShowsId = showId1,
            Actor = actor1,
            ActorsId = actorId1
        };

        // Modify properties
        actorShow.Show = show2;
        actorShow.ShowsId = showId2;
        actorShow.Actor = actor2;
        actorShow.ActorsId = actorId2;

        // Assert
        Assert.Equal(show2, actorShow.Show);
        Assert.Equal(showId2, actorShow.ShowsId);
        Assert.Equal(actor2, actorShow.Actor);
        Assert.Equal(actorId2, actorShow.ActorsId);
    }
}