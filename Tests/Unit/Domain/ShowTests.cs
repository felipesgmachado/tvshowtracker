﻿using Domain.Shows;
using Type = Domain.Shows.Type;

namespace Tests.Unit.Domain;
public class ShowTests
{          
    [Fact]
    public void ShowConstructor_WithValidArguments_ShouldCreateShowObject()
    {
        // Arrange
        string name = "Test Show";
        string details = "Test details";
        DateTime releaseDate = DateTime.Now;
        Genre genre = Genre.Action;
        Type type = Type.Movies;

        // Act
        Show show = new Show(name, details, releaseDate, genre, type);

        // Assert
        Assert.NotNull(show);
        Assert.Equal(name, show.Name);
        Assert.Equal(details, show.Details);
        Assert.Equal(releaseDate, show.ReleaseDate);
        Assert.Equal(genre, show.Genre);
        Assert.Equal(type, show.Type);
        Assert.NotNull(show.Actors);
        Assert.NotNull(show.Seasons);
    }

    [Theory]
    [InlineData("", "Test details", "2023-01-01", Genre.Comedy, Type.Series)]
    [InlineData("Test Show", "", "2023-01-01", Genre.Comedy, Type.Series)]
    public void ShowConstructor_WithInvalidArguments_ShouldThrowArgumentException(
        string name, string details, string releaseDateString, Genre genre, Type type)
    {
        // Arrange
        DateTime releaseDate = DateTime.Parse(releaseDateString);

        // Act & Assert
        Assert.Throws<ArgumentException>(() => new Show(name, details, releaseDate, genre, type));
    }   

    [Fact]
    public void ShowId_ShouldBeGuid()
    {
        // Arrange
        Show show = new Show("Test Show", "Test details", DateTime.Now, Genre.Action, Type.Movies);

        // Assert
        Assert.IsType<Guid>(show.Id);
    }

}