﻿using Domain.Actors;

namespace Tests.Unit.Domain;

public class ActorTests
{
    [Fact]
    public void ActorConstructor_WithValidArguments_ShouldCreateActorObject()
    {
        // Arrange
        string actorName = "John Doe";

        // Act
        Actor actor = new Actor(actorName);

        // Assert
        Assert.NotNull(actor);
        Assert.NotEqual(Guid.Empty, actor.Id);
        Assert.Equal(actorName, actor.Name);
        Assert.NotNull(actor.Shows);
        Assert.Empty(actor.Shows);
    }

    [Theory]
    [InlineData("")]
    public void ActorConstructor_WithInvalidName_ShouldThrowArgumentException(string invalidName)
    {
        // Act & Assert
        Assert.Throws<ArgumentException>(() => new Actor(invalidName));
    }
}
