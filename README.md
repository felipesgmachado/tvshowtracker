## Introduction

In this project, a RESTful API called TVShowTracker was developed in .NET 8 using aproaches as a clean architecture and Domain Driven Design.

For authentication and authorization, JWT and a custom class were used that uses IdentityUser to create a relationship with the domain User.

Among some patterns that were used in development, there are some that are more evident, such as the CQS pattern, which was used to partially separate the query flow from the flow that makes changes to the database. The Unit of Work pattern was also used to perform transitions in the application and the repository pattern to ensure less coupling.

EntityFrameworkCore was used as the ORM and SqlServer as the database.

The database was created using code first, generating migrations and updating the database

After that, the database was fully populated using the API itself and some Jsons files that are available in the Infrastructure layer.

I believe that the point that I took most into consideration when developing this project was being able to provide easy maintenance. Maybe that's why I didn't complete everything I set out to do.

It is important to say that this is a project that, although it was tiring to develop within the given deadline. It's a project that I found very interesting and I intend to continue development, completing the remaining items and improving where possible.

## Prerequisites:
Install .NET 8 SDK:
Ensure that you have the .NET 8 SDK installed on your machine. You can download it from the official Microsoft website: [Download .NET](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)

Integrated Development Environment (IDE):
Use an IDE of your choice, such as Visual Studio or Visual Studio Code.

## Execution Steps:

1. Clone the Repository: 
Clone the project repository to your machine.

`git clone https://gitlab.com/felipesgmachado/tvshowtracker.git`


2. Open the Project in the IDE:
Open the project using your preferred IDE.

**Visual Studio Code**
: Go to the project solution folder and type 
`code .`

**Visual Studio**:
Open Visual Studio and select "File" > "Open" > "Project/Solution," then choose the TVShowTracker project's solution file (.sln).

3. Restore Dependencies:
Open a terminal in the project folder and execute the following command to restore dependencies.
`dotnet restore`

4. Configure Database:
If your project connects to a database, check and configure the connection string in the corresponding configuration file. 
On Visual Studio go to tools -> NuGet Package Manager -> Package Manager Console and type `update-database`

5. Run the Project:
Execute the following command to start the application inside the API project folder: `dotnet run`.
This will start the application and make the Web API available at a specific endpoint http://localhost:5085. 

6. Test the Web API:
Open a browser or an API testing tool like Postman and test your Web API endpoints.

7. Stop the Application:
To stop the application, press Ctrl + C in the terminal where the application is running.

### Links to endpoints

[Swagger http endpoints](http://localhost:5085/swagger/index.html)

[Swagger https endpoints](https://localhost:7248/swagger/index.html)


## More details about the project

Use the endpoints and the json files available on Infrastructure\JsonFiles

Those json files contains informations about TV Shows and was generated using chat GPT.

I will include a postam collection soon.


## Technical Requirements

 - [.NET 8 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)
 - [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)

