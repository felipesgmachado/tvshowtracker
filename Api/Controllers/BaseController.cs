﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Api.Controllers;

[ApiController]
public class BaseController : ControllerBase
{
    protected ActionResult CustomResponse(HttpStatusCode statusCode, object result, int? total = null)
    {
        if (HttpStatusCode.OK == statusCode)
        {
            return Ok(new
            {
                data = result,
                total
            });
        }

        return BadRequest(new
        {
            data = new { },
            total
        });
    }
}
