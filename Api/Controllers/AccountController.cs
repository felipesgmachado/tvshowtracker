﻿using ApplicationCore.Users.Auth.LoginUser;
using ApplicationCore.Users.Auth.RegisterUser;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[Route("api/auth")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly IRegisterUserUseCase _registerUserUseCase;
    private readonly ILoginUserUseCase _loginUserUseCase;

    public AuthController(IRegisterUserUseCase registerUserUseCase, ILoginUserUseCase loginUserUseCase)
    {
        _registerUserUseCase = registerUserUseCase;
        _loginUserUseCase = loginUserUseCase;
    }

    [HttpPost]
    [Route("register", Name = "Register")]
    public async Task<IActionResult> RegisterAsync(RegisterUserInput input)
    {
        var result = await _registerUserUseCase.Handle(input);

        if (result.IsError)
            return BadRequest();

        return Ok(result);
    }

    [HttpPost]
    [Route("login", Name = "Login")]
    public async Task<IActionResult> LoginAsync(LoginUserInput input)
    {
        var result = await _loginUserUseCase.Handle(input);

        if (result.IsError)
            return BadRequest();

        return Ok(result);
    }
}
