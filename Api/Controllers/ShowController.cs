﻿using ApplicationCore.Shared;
using ApplicationCore.Shows.AddShow;
using ApplicationCore.Shows.GetActorsByShowName;
using ApplicationCore.Shows.GetAllShows;
using ApplicationCore.Shows.GetShowsByGenre;
using ApplicationCore.Shows.GetShowsByType;
using ApplicationCore.Shows.GetShowsRecomendationsByFavorite;
using Domain.Shows;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;
using Type = Domain.Shows.Type;

namespace Api.Controllers;

[Route("api/show")]
[Authorize]
public class ShowController : BaseController
{
    private readonly IAddShowUseCase _addShowUseCase;
    private readonly IGetShowsByGenreUseCase _getShowsByGenreUseCase;
    private readonly IGetShowsByTypeUseCase _getShowsByTypeUseCase;
    private readonly IGetActorsByShowNameUseCase _getActorsByShowNameUseCase;
    private readonly IGetAllShowsUseCase _getAllShowsUseCase;
    private readonly IGetShowsRecomendationsByFavoriteUseCase _getShowsRecomendationsByFavoriteUseCase;

    public ShowController(IAddShowUseCase addShowUseCase, IGetShowsByGenreUseCase getShowsByGenreUseCase, 
        IGetShowsByTypeUseCase getShowsByTypeUseCase, IGetActorsByShowNameUseCase getActorsByShowNameUseCase, 
        IGetAllShowsUseCase getAllShowsUseCase, IGetShowsRecomendationsByFavoriteUseCase getShowsRecomendationsByFavoriteUseCase)
    {
        _addShowUseCase = addShowUseCase;
        _getShowsByGenreUseCase = getShowsByGenreUseCase;
        _getShowsByTypeUseCase = getShowsByTypeUseCase;
        _getActorsByShowNameUseCase = getActorsByShowNameUseCase;
        _getAllShowsUseCase = getAllShowsUseCase;
        _getShowsRecomendationsByFavoriteUseCase = getShowsRecomendationsByFavoriteUseCase;
    }

    [HttpGet]
    [Route("", Name = "GetAllShows")]
    public async Task<IActionResult> GetShowsAsync([FromQuery] BaseInput input)
    {        
        var (total, result) = await _getAllShowsUseCase.Handle(input);

        if (result.IsError)
            return CustomResponse(HttpStatusCode.BadRequest, result);

        return CustomResponse(HttpStatusCode.OK, result, total);
    }

    [HttpGet]
    [Route("ShowsByGenre/{genre}", Name = "GetShowsByGenre")]
    public async Task<IActionResult> GetShowsByGenreAsync([FromQuery] BaseInput input, Genre genre)
    {
        var (total, result) = await _getShowsByGenreUseCase.Handle(genre, input);

        if (result.IsError)
            return CustomResponse(HttpStatusCode.BadRequest, result);

        return CustomResponse(HttpStatusCode.OK, result, total);
    }

    [HttpGet]
    [Route("ShowsByType/{type}", Name = "GetShowsByType")]
    public async Task<IActionResult> GetShowsByTypeAsync([FromQuery] BaseInput input, Type type)
    {
        var (total, result) = await _getShowsByTypeUseCase.Handle(type, input);

        if (result.IsError)
            return CustomResponse(HttpStatusCode.BadRequest, result);

        return CustomResponse(HttpStatusCode.OK, result, total);
    }

    [HttpGet]
    [Route("ShowsRecomendations/", Name = "GetShowsRecomendationsByFavorite")]
    public async Task<IActionResult> GetShowsRecomendationsByFavoriteAsync(bool activateBackgroundService)
    {
        var user = User?.Claims.Where(x => x.Type == ClaimTypes.Email).FirstOrDefault()?.Value;
        var result = await _getShowsRecomendationsByFavoriteUseCase.Handle(user, activateBackgroundService);

        if (result.IsError)
            return BadRequest();

        return Ok(result);
    }

    [HttpGet]
    [Route("ActorsByShow/{showName}", Name = "GetActorsByShow")]
    public async Task<IActionResult> GetActorsByShowAsync([FromRoute] string showName)
    {
        var result = await _getActorsByShowNameUseCase.Handle(showName);

        if (result.IsError)
            return BadRequest();

        return Ok(result);
    }

    [HttpPost]
    [Route("", Name = "AddShow")]
    public async Task<IActionResult> AddShowAsync(AddShowInput input)
    {
        var result = await _addShowUseCase.Handle(input);

        if (result.IsError)
            return BadRequest();

        return Ok(result);
    }
}
