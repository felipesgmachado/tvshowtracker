﻿using ApplicationCore.Favorites.AddFavorite;
using ApplicationCore.Favorites.GetFavorites;
using ApplicationCore.Favorites.RemoveFavorite;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Api.Controllers
{
    [Route("api/favorites")]
    [Authorize]
    [ApiController]
    public class FavoritesController : ControllerBase
    {
        private readonly IAddFavoriteUserCase _addFavoriteUserCase;
        private readonly IRemoveFavoriteUseCase _removeFavoriteUserCase;
        private readonly IGetFavoritesUseCase _getFavoritesUseCase;        

        public FavoritesController(IAddFavoriteUserCase addFavoriteUserCase, IRemoveFavoriteUseCase removeFavoriteUserCase, 
            IGetFavoritesUseCase getFavoritesUseCase)
        {
            _addFavoriteUserCase = addFavoriteUserCase;
            _removeFavoriteUserCase = removeFavoriteUserCase;
            _getFavoritesUseCase = getFavoritesUseCase;
        }

        [HttpGet]
        [Route("", Name = "GetFavorites")]
        public async Task<IActionResult> GetFavoritesAsync()
        {
            var user = User?.Claims.Where(x => x.Type == ClaimTypes.Email).FirstOrDefault()?.Value;
            var result = await _getFavoritesUseCase.Handle(user);

            if (result.IsError)
                return BadRequest();

            return Ok(result);
        }

        [HttpDelete]
        [Route("", Name = "RemoveFavorite")]
        public async Task<IActionResult> RemoveFavoriteAsync(RemoveFavoriteInput input)
        {
            var user = User?.Claims.Where(x => x.Type == ClaimTypes.Email).FirstOrDefault()?.Value;
            var result = await _removeFavoriteUserCase.Handle(input, user);

            if (result.IsError)
                return BadRequest();

            return Ok(result);
        }

        [HttpPost]
        [Route("", Name = "AddFavorite")]
        public async Task<IActionResult> AddFavoriteAsync(AddFavoriteInput input)
        {
            var user = User?.Claims.Where(x => x.Type == ClaimTypes.Email).FirstOrDefault()?.Value;
            var result = await _addFavoriteUserCase.Handle(input, user);

            if (result.IsError)
                return BadRequest();

            return Ok(result);
        }
    }
}
