﻿using Infrastructure.Services.BackGround;
using Infrastructure.Services.Email;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/email")]
    [Authorize]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;       

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;            
        }

        [HttpPost]
        public async Task<IActionResult> SendEmailAsync(Message request)
        {
            var result = await _emailService.SendEmailAsync(request);

            if (result.IsError)
                return BadRequest();

            return Ok(result);
        }

        [HttpGet]
        [Route("StateBackgroundService/", Name = "GetServiceState")]
        public ActionResult<BackgroundHostedServiceState> GetServiceState([FromServices] IServiceScopeFactory serviceScopeFactory)
        {
            using (var scope = serviceScopeFactory.CreateScope())
            {
                var service = scope.ServiceProvider.GetRequiredService<BackgroundHostedService>();
                return Ok(new BackgroundHostedServiceState(service.IsEnabled));
            }            
        }

        [HttpPatch]
        [Route("StartBackgroundService/", Name = "SetServiceState")]
        public IActionResult SetStateService(BackgroundHostedServiceState state, BackgroundHostedService service)
        {
            service.IsEnabled = state.IsEnabled;
            return Ok();
        }

    }
}
