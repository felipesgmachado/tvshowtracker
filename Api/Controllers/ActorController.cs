﻿using ApplicationCore.Actors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/actor")]
    [Authorize]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly IGetShowsByActorNameUseCase _getShowsByActorNameUseCase;

        public ActorController(IGetShowsByActorNameUseCase getShowsByActorNameUseCase)
        {
            _getShowsByActorNameUseCase = getShowsByActorNameUseCase;
        }

        [HttpGet]
        [Route("ShowsByActor/{actorName}", Name = "GetShowsByActor")]
        public async Task<IActionResult> GetShowsByActorAsync([FromRoute] string actorName)
        {
            var result = await _getShowsByActorNameUseCase.Handle(actorName);

            if (result.IsError)
                return BadRequest();

            return Ok(result);
        }
    }
}
