using ApplicationCore.Actors;
using ApplicationCore.Favorites.AddFavorite;
using ApplicationCore.Favorites.GetFavorites;
using ApplicationCore.Favorites.RemoveFavorite;
using ApplicationCore.Shows.AddShow;
using ApplicationCore.Shows.GetActorsByShowName;
using ApplicationCore.Shows.GetAllShows;
using ApplicationCore.Shows.GetShowsByGenre;
using ApplicationCore.Shows.GetShowsByType;
using ApplicationCore.Shows.GetShowsRecomendationsByFavorite;
using ApplicationCore.Users.Auth.LoginUser;
using ApplicationCore.Users.Auth.RegisterUser;
using Domain.Actors;
using Domain.Episodes;
using Domain.Favorites;
using Domain.Seasons;
using Domain.Shows;
using Domain.Users;
using Infrastructure.Context;
using Infrastructure.Repositories;
using Infrastructure.Services.Auth;
using Infrastructure.Services.BackGround;
using Infrastructure.Services.Email;
using Infrastructure.Services.Transations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

//builder.Services.AddHostedService<BackgroundTaskService>();

//builder.Services.AddScoped<SampleService>();

builder.Services.AddScoped<EmailService>();
builder.Services.AddSingleton<BackgroundHostedService>();
builder.Services.AddHostedService(provider => provider.GetRequiredService<BackgroundHostedService>());

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

var emailConfig = builder.Configuration
        .GetSection("EmailConfiguration")
        .Get<EmailConfiguration>();
builder.Services.AddSingleton(emailConfig);

builder.Services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                }); ;

builder.Services.AddCors(o => o.AddPolicy("default", builder =>
{
    builder.AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader();
}));


builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "API Documentation",
        Description = "API Documentation",
    });

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "Insert the JWT : Bearer {your token}",
        Name = "Authorization",
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey
    });

    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },

            new string[] {}
        }
    });
});

var conSqlServer = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(conSqlServer));
builder.Services.AddIdentityApiEndpoints<IdentityUser>().AddEntityFrameworkStores<AppDbContext>();

builder.Services.AddAuthorization();
builder.Services.AddAuthentication(x =>
{
    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("#f44(*()#&hc138vg3vggev82747t(&tEFGU24GFIY2GFGEVIUEVOWEVUWGECIYQE")),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IShowRepository, ShowRepository>();
builder.Services.AddScoped<ISeasonRepository, SeasonRepository>();
builder.Services.AddScoped<IEpisodeRepository, EpisodeRepository>();
builder.Services.AddScoped<IActorRepository, ActorRepository>();
builder.Services.AddScoped<IFavoriteRepository, FavoriteRepository>();

builder.Services.AddScoped<IAuthService, AuthService>();
builder.Services.AddScoped<IEmailService, EmailService>();

builder.Services.AddScoped<IRegisterUserUseCase, RegisterUserUseCase>();
builder.Services.AddScoped<ILoginUserUseCase, LoginUserUseCase>();
builder.Services.AddScoped<IAddShowUseCase, AddShowUseCase>();
builder.Services.AddScoped<IGetActorsByShowNameUseCase, GetActorsByShowNameUseCase>();
builder.Services.AddScoped<IGetAllShowsUseCase, GetAllShowsUseCase>();
builder.Services.AddScoped<IGetShowsByGenreUseCase, GetShowsByGenreUseCase>();
builder.Services.AddScoped<IGetShowsByTypeUseCase, GetShowsByTypeUseCase>();
builder.Services.AddScoped<IGetShowsByActorNameUseCase, GetShowsByActorNameUseCase>();
builder.Services.AddScoped<IAddFavoriteUserCase, AddFavoriteUserCase>();
builder.Services.AddScoped<IRemoveFavoriteUseCase, RemoveFavoriteUseCase>();
builder.Services.AddScoped<IGetFavoritesUseCase, GetFavoritesUseCase>();
builder.Services.AddScoped<IGetShowsRecomendationsByFavoriteUseCase, GetShowsRecomendationsByFavoriteUseCase>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
//app.MapGroup("/identity").MapIdentityApi<IdentityUser>();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();

