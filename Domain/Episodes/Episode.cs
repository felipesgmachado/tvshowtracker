﻿namespace Domain.Episodes;

public class Episode
{
    public Episode(Guid seasonId, int number, string title, string details, DateTime releaseDate, int duration)
    {
        Id = Guid.NewGuid();

        ArgumentException.ThrowIfNullOrEmpty(title);
        ArgumentException.ThrowIfNullOrEmpty(details);
        ArgumentNullException.ThrowIfNull(duration);
        ArgumentNullException.ThrowIfNull(number);

        if (seasonId == Guid.Empty)
            throw new ArgumentNullException(nameof(seasonId));

        if (releaseDate == default)
            throw new ArgumentNullException(nameof(releaseDate), "ReleaseDate cannot be the default DateTime value.");

        SeasonId = seasonId;
        Number = number;
        Title = title;
        ReleaseDate = releaseDate;
        Details = details;
        Duration = duration;
    }

    public Guid Id { get; private set; }
    public int Number { get; private set; }
    public string Title { get; private set; }
    public string Details { get; private set; }
    public DateTime ReleaseDate { get; private set; }
    public int Duration { get; private set; }
    public Guid SeasonId { get; private set; }
}