﻿namespace Domain.Episodes;

public interface IEpisodeRepository
{
    Task InsertAsync(Episode episode);
}
