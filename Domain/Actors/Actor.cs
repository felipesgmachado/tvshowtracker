﻿using Domain.Shows;

namespace Domain.Actors;

public class Actor
{
    public Actor(string name)
    {
        Id = Guid.NewGuid();
        
        ArgumentException.ThrowIfNullOrEmpty(name);

        Name = name;
        Shows = new List<Show>();
    }

    public Guid Id { get; private set; }
    public string Name { get; private set; }
    public List<Show> Shows { get; private set; }
}
