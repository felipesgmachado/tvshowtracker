﻿using Domain.Episodes;
using Domain.Shows;
using Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Actors
{
    public interface IActorRepository
    {
        Task InsertAsync(Actor actor);
        Task<Actor> GetByNameAsync(string name);
        Task<Actor> GetShowsByActorAsync(string name);
        Task UpdateAsync(Actor actor);
    }
}
