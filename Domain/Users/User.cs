﻿using Domain.Favorites;

namespace Domain.Users;

public class User
{
    public User(string name, string email, bool isRGPDCompliance)
    {
        Id = Guid.NewGuid();
        CreatedOnUtc = DateTime.UtcNow;

        ArgumentException.ThrowIfNullOrEmpty(name);
        ArgumentException.ThrowIfNullOrEmpty(email);

        Name = name;
        Email = email;
        IsRGPDCompliance = isRGPDCompliance;
    }

    public Guid Id { get; private set; }
    public string Name { get; private set; }
    public string Email { get; private set; }
    public DateTime CreatedOnUtc { get; private set; }
    public List<Favorite>? Favorites { get; private set; }
    public bool IsRGPDCompliance { get; private set; }
}

