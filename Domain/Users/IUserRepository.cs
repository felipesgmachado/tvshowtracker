﻿namespace Domain.Users;

public interface IUserRepository
{
    Task<User?> GetByIdAsync(Guid id);
    Task<User?> GetByEmailAsync(string email);
    Task<User?> GetByUsernameAsync(string username);
    Task<Guid> InsertAsync(User user);
    Task UpdateAsync(User user);
    Task DeleteAsync(User user);
}
