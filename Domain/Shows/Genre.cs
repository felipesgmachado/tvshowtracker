﻿namespace Domain.Shows;

public enum Genre
{
    Drama = 0,
    Fantasy = 1,
    Horror = 2,
    Crime = 3,
    Thriller = 4,
    Action = 5,
    Adventure = 6,
    SciFi = 7,
    Biography = 8,
    History = 9,
    Comedy = 10,
    Mystery = 11,
    Animation = 12,
    Romance = 13,
}
