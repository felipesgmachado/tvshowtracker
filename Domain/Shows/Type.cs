﻿namespace Domain.Shows;

public enum Type
{
    Movies = 0,
    Series = 1
}
