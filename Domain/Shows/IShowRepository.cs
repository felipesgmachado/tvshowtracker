﻿using Domain.Actors;

namespace Domain.Shows;

public interface IShowRepository
{
    Task<(int, IEnumerable<Show>)> GetAllShowsAsync(int pageSize, int pageIndex, string? orderBy);
    Task<Show> GetByIdAsync(Guid id);
    Task<Show> GetByNameAsync(string name);
    Task<IEnumerable<Show>> GetWithNameAsync(string name);
    Task<(int, IEnumerable<Show>)> GetByGenreAsync(Genre genre, int pageSize, int pageIndex, string? orderBy);
    Task<IEnumerable<Show>> GetByGenreAsync(Genre genre);
    Task<(int, IEnumerable<Show>)> GetByTypeAsync(Type type, int pageSize, int pageIndex, string? orderBy);
    Task<Show> GetActorsByShowAsync(string name);
    Task<IEnumerable<Show>> GetShowsWithActor(string name);
    Task InsertAsync(Show show);
    Task UpdateAsync(Show show);
}
