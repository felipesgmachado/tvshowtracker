﻿using Domain.Actors;

namespace Domain.Shows;

public class ActorShow
{
    public Show Show { get; set; }
    public Guid ShowsId { get; set; }
    public Actor Actor { get; set; }
    public Guid ActorsId { get; set; }
}
