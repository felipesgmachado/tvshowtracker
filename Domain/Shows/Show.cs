﻿using Domain.Actors;
using Domain.Seasons;

namespace Domain.Shows;

public class Show
{
    public Show(string name, string details, DateTime releaseDate, Genre genre, Type type)
    {
        Id = Guid.NewGuid();
        
        ArgumentException.ThrowIfNullOrEmpty(name);
        ArgumentException.ThrowIfNullOrEmpty(details);
        ArgumentException.ThrowIfNullOrEmpty(genre.ToString());
        ArgumentException.ThrowIfNullOrEmpty(type.ToString());
        if (releaseDate == default)
            throw new ArgumentNullException(nameof(releaseDate), "ReleaseDate cannot be the default DateTime value.");

        Name = name;
        Details = details;
        ReleaseDate = releaseDate;
        Genre = genre;
        Type = type;
        Actors = new List<Actor>();
        Seasons = new List<Season>();       
    }

    public Guid Id { get; private set; }
    public string Name { get; private set; }
    public DateTime ReleaseDate { get; private set; }
    public string Details { get; private set; }    
    public Genre Genre { get; private set; }
    public Type Type { get; private set; }
    public List<Actor> Actors { get; private set; }
    public List<Season> Seasons { get; private set; }    
}