﻿namespace Domain.Seasons;

public interface ISeasonRepository
{
    Task InsertAsync(Season season);
}
