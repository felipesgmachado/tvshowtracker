﻿using Domain.Episodes;

namespace Domain.Seasons;

public class Season
{
    public Season(Guid showId, string name, string details, DateTime releaseDate)
    {
        Id = Guid.NewGuid();

        if (showId == Guid.Empty)
            throw new ArgumentNullException(nameof(showId), "ShowId cannot be empty.");

        if (string.IsNullOrEmpty(name))
            throw new ArgumentException("Season name cannot be null or empty.", nameof(name));

        if (string.IsNullOrEmpty(details))
            throw new ArgumentException("Season details cannot be null or empty.", nameof(details));

        if (releaseDate == default)
            throw new ArgumentNullException(nameof(releaseDate), "ReleaseDate cannot be the default DateTime value.");

        ShowId = showId;
        Episode = new List<Episode>();
        Name = name;
        ReleaseDate = releaseDate;
        Details = details;
    }

    public Guid Id { get; private set; }
    public string Name { get; private set; }
    public DateTime ReleaseDate { get; private set; }
    public string Details { get; private set; }
    public List<Episode> Episode { get; private set; }
    public Guid ShowId { get; private set; }
}