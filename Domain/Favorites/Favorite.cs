﻿namespace Domain.Favorites;

public class Favorite
{
    public Favorite(Guid showId, Guid userId)
    {
        Id = Guid.NewGuid();

        ArgumentException.ThrowIfNullOrWhiteSpace(showId.ToString());
        ArgumentException.ThrowIfNullOrWhiteSpace(userId.ToString());       

        ShowId = showId;
        UserId = userId;
    }

    public Guid Id { get; private set; }
    public Guid ShowId { get; private set; }
    public Guid UserId { get; private set; }
}
