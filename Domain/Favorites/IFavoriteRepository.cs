﻿using Domain.Shows;
using Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Favorites
{
    public interface IFavoriteRepository
    {
        Task<Favorite?> GetByIdAsync(Guid id);
        Task<IEnumerable<Favorite?>> GetByUserIdAsync(Guid userId);
        Task<Favorite?> GetByShowAsync(Guid showId);
        Task InsertAsync(Favorite favorite);
        Task UpdateAsync(Favorite favorite);
        Task DeleteAsync(Favorite favorite);
    }
}
