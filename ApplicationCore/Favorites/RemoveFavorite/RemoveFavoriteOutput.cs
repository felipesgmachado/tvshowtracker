﻿using ApplicationCore.Shared;
using Domain.Favorites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.RemoveFavorite
{
    public class RemoveFavoriteOutput : BaseOutput
    {
        public RemoveFavoriteOutput()
        {
                
        }

        public RemoveFavoriteOutput(Favorite entity)
        {
            IsError = false;

            FavoriteId = entity.Id;
        }

        public Guid FavoriteId { get; set; }
    }
}
