﻿using ApplicationCore.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.RemoveFavorite
{
    public class RemoveFavoriteInput : BaseInput
    {
        public string ShowName { get; set; }
    }
}
