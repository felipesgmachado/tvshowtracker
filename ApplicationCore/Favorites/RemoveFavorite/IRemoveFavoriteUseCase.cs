﻿using ApplicationCore.Favorites.AddFavorite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.RemoveFavorite
{
    public interface IRemoveFavoriteUseCase
    {
        Task<RemoveFavoriteOutput> Handle(RemoveFavoriteInput input, string userName);
    }
}
