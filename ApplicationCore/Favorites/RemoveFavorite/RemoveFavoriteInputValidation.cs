﻿using ApplicationCore.Favorites.AddFavorite;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.RemoveFavorite
{
    public class RemoveFavoriteInputValidation : AbstractValidator<RemoveFavoriteInput>
    {
        public RemoveFavoriteInputValidation()
        {
            RuleFor(c => c.ShowName).NotEmpty().WithMessage("ShowName is required");
        }
    }
}
