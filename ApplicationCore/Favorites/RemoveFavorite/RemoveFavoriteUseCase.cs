﻿using ApplicationCore.Favorites.AddFavorite;
using ApplicationCore.Shared;
using Domain.Favorites;
using Domain.Shows;
using Domain.Users;

namespace ApplicationCore.Favorites.RemoveFavorite
{
    public class RemoveFavoriteUseCase : BaseUseCase, IRemoveFavoriteUseCase
    {
        private readonly IShowRepository _showRepository;
        private readonly IUserRepository _userRepository;
        private readonly IFavoriteRepository _favoriteRepository;

        public RemoveFavoriteUseCase(IShowRepository showRepository, IUserRepository userRepository, IFavoriteRepository favoriteRepository)
        {
            _showRepository = showRepository;
            _userRepository = userRepository;
            _favoriteRepository = favoriteRepository;
        }

        public async Task<RemoveFavoriteOutput> Handle(RemoveFavoriteInput input, string userName)
        {
            if (!IsValid(new RemoveFavoriteInputValidation(), input))
                return new RemoveFavoriteOutput();

            var user = await _userRepository.GetByEmailAsync(userName);

            if (user == null)
                return new RemoveFavoriteOutput();

            var show = await _showRepository.GetByNameAsync(input.ShowName);

            if (show == null)
                return new RemoveFavoriteOutput();            

            var favorite = await _favoriteRepository.GetByShowAsync(show.Id);

            if (favorite == null)
                return new RemoveFavoriteOutput();

            await _favoriteRepository.DeleteAsync(favorite);

            return new RemoveFavoriteOutput(favorite);
        }
    }
}
