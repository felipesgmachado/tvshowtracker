﻿using ApplicationCore.Shared;
using Domain.Favorites;
using Domain.Shows;
using Domain.Users;

namespace ApplicationCore.Favorites.AddFavorite;

public class AddFavoriteUserCase : BaseUseCase, IAddFavoriteUserCase
{
    private readonly IShowRepository _showRepository;
    private readonly IUserRepository _userRepository;
    private readonly IFavoriteRepository _favoriteRepository;

    public AddFavoriteUserCase(IShowRepository showRepository, IUserRepository userRepository, IFavoriteRepository favoriteRepository)
    {
        _showRepository = showRepository;
        _userRepository = userRepository;
        _favoriteRepository = favoriteRepository;
    }

    public async Task<AddFavoriteOutput> Handle(AddFavoriteInput input, string userName)
    {
        if (!IsValid(new AddFavoriteInputValidation(), input))
            return new AddFavoriteOutput();

        var user = await _userRepository.GetByEmailAsync(userName);

        if (user == null)
            return new AddFavoriteOutput();

        var showResult = await _showRepository.GetByNameAsync(input.ShowName);

        if (showResult == null)
            return new AddFavoriteOutput();

        var newFavorite = new Favorite(showResult.Id, user.Id);

        var favorite = await _favoriteRepository.GetByShowAsync(newFavorite.ShowId);

        if (favorite == null)
            return new AddFavoriteOutput();

        await _favoriteRepository.InsertAsync(newFavorite);

        return new AddFavoriteOutput(newFavorite);
    }
}
