﻿using ApplicationCore.Shows.AddShow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.AddFavorite
{
    public interface IAddFavoriteUserCase
    {
        Task<AddFavoriteOutput> Handle(AddFavoriteInput input, string userName);
    }
}
