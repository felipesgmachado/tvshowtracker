﻿using ApplicationCore.Shared;
using Domain.Favorites;
using Domain.Shows;
using Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.AddFavorite
{
    public class AddFavoriteOutput : BaseOutput
    {
        public AddFavoriteOutput()
        {

        }

        public AddFavoriteOutput(Favorite entity)
        {
            IsError = false;

            FavoriteId = entity.Id;            
        }

        public Guid FavoriteId { get; set; }        
    }
}
