﻿using FluentValidation;

namespace ApplicationCore.Favorites.AddFavorite
{
    public class AddFavoriteInputValidation : AbstractValidator<AddFavoriteInput>
    {
        public AddFavoriteInputValidation()
        {
            RuleFor(c => c.ShowName).NotEmpty().WithMessage("ShowName is required");
        }
    }
}
