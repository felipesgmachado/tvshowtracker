﻿using ApplicationCore.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.AddFavorite
{
    public class AddFavoriteInput : BaseInput
    {
        public string ShowName { get; set; }
    }
}
