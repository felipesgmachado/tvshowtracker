﻿using ApplicationCore.Favorites.AddFavorite;
using ApplicationCore.Shared;
using ApplicationCore.Shows.GetAllShows;
using Domain.Favorites;
using Domain.Shows;
using Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.GetFavorites
{
    public class GetFavoritesUseCase : BaseUseCase, IGetFavoritesUseCase
    {
        private readonly IShowRepository _showRepository;
        private readonly IUserRepository _userRepository;
        private readonly IFavoriteRepository _favoriteRepository;

        public GetFavoritesUseCase(IShowRepository showRepository, IUserRepository userRepository, IFavoriteRepository favoriteRepository)
        {
            _showRepository = showRepository;
            _userRepository = userRepository;
            _favoriteRepository = favoriteRepository;
        }

        public async Task<GetFavoritesOutput> Handle(string userName)
        {           
            var user = await _userRepository.GetByEmailAsync(userName);

            if (user == null)
                return new GetFavoritesOutput();            

            var favorites = await _favoriteRepository.GetByUserIdAsync(user.Id);

            if (favorites == null) 
                return new GetFavoritesOutput();

            var showsList = new List<Show>();

            foreach (var item in favorites)
            {
                var show = await _showRepository.GetByIdAsync(item.ShowId);
                showsList.Add(show);
            }

            return new GetFavoritesOutput(user, showsList);
        }
    }
}
