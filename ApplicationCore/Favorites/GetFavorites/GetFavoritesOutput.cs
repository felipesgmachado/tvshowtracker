﻿using ApplicationCore.Shared;
using Domain.Shows;
using Domain.Users;

namespace ApplicationCore.Favorites.GetFavorites
{
    public class GetFavoritesOutput : BaseOutput
    {
        public GetFavoritesOutput()
        {
            
        }

        public GetFavoritesOutput(User user, List<Show> shows)
        {
            IsError = false;

            User = user.Name;
            ShowNames = shows.Select(s => s.Name).ToList();
        }

        public string User { get; set; }
        public List<string> ShowNames { get; set; }
    }
}
