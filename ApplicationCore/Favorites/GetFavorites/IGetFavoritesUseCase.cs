﻿using ApplicationCore.Shared;
using ApplicationCore.Shows.GetAllShows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Favorites.GetFavorites
{
    public interface IGetFavoritesUseCase
    {
        Task<GetFavoritesOutput> Handle(string userName);
    }
}
