﻿using ApplicationCore.Shared;
using Domain.Actors;

namespace ApplicationCore.Actors
{
    public class GetShowsByActorNameUseCase : BaseUseCase, IGetShowsByActorNameUseCase
    {
        private readonly IActorRepository actorRepository;

        public GetShowsByActorNameUseCase(IActorRepository actorRepository)
        {
            this.actorRepository = actorRepository;
        }

        public async Task<GetShowsByActorNameOutput> Handle(string input)
        {
            var actor = await actorRepository.GetShowsByActorAsync(input);

            if (actor == null)
                return new GetShowsByActorNameOutput();

            return new GetShowsByActorNameOutput(actor);
        }
    }
}
