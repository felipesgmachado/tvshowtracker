﻿using ApplicationCore.Shows.GetActorsByShowName;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Actors
{
    public interface IGetShowsByActorNameUseCase
    {
        Task<GetShowsByActorNameOutput> Handle(string input);
    }
}
