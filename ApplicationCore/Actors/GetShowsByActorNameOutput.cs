﻿using ApplicationCore.Shared;
using ApplicationCore.Shows.Shared;
using Domain.Actors;
using Domain.Shows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Actors
{
    public class GetShowsByActorNameOutput : BaseOutput
    {
        public GetShowsByActorNameOutput()
        {
                          
        }

        public GetShowsByActorNameOutput(Actor entity)
        {
            IsError = false;

            //Id = entity.Id;
            Name = entity.Name;
            Shows = new List<ActorShowOutput>();

            foreach (var show in entity.Shows)
            {
                var actorShow = new ActorShowOutput()
                {
                    Name = show.Name,
                    //Details = show.Details,
                    //ReleaseDate = show.ReleaseDate,
                    //Genre = show.Genre.ToString(),
                    //Type = show.Type.ToString(),
                };

                Shows.Add(actorShow);
            }                       
        }

        //public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ActorShowOutput> Shows { get; set; }
    }

    public class ActorShowOutput 
    {
        public string Name { get; set; }
        //public string Details { get; set; }
        //public DateTime ReleaseDate { get; set; }
        //public string Genre { get; set; }
        //public string Type { get; set; }
    }
}
