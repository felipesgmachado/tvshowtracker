﻿using ApplicationCore.Shared;
using ApplicationCore.Shows.Shared;
using Domain.Shows;

namespace ApplicationCore.Shows.GetShowsRecomendationsByFavorite;

public class GetShowsRecomendationsByFavoriteOutput : BaseOutput
{
    public GetShowsRecomendationsByFavoriteOutput()
    {

    }

    public GetShowsRecomendationsByFavoriteOutput(List<Show> entities, bool isBackgroundServiceActive)
    {
        IsError = false;

        Shows = new List<ShowOutput>();

        foreach (var entity in entities)
        {
            var seasons = new List<ShowSeasonOutput>();

            var show = new ShowOutput()
            {
                Name = entity.Name,
                Details = entity.Details,
                ReleaseDate = entity.ReleaseDate,
                Genre = entity.Genre.ToString(),
                Type = entity.Type.ToString(),
                Actors = entity.Actors.Select(a => a.Name).ToList()
            };

            foreach (var season in entity.Seasons)
            {
                if (entity.Type == 0)
                    continue;

                var episodes = new List<ShowSeasonEpisodeOutput>();

                var showSeason = new ShowSeasonOutput()
                {
                    Name = season.Name,
                    Details = season.Details,
                    ReleaseDate = season.ReleaseDate
                };

                foreach (var episode in season.Episode)
                {
                    var showSeasonEpisode = new ShowSeasonEpisodeOutput()
                    {
                        Title = episode.Title,
                        Number = episode.Number,
                        Details = episode.Details,
                        ReleaseDate = episode.ReleaseDate,
                        Duration = episode.Duration
                    };

                    episodes.Add(showSeasonEpisode);
                }

                showSeason.Episodes = episodes.OrderBy(x => x.Number).ToList();
                seasons.Add(showSeason);
            }

            show.Seasons = seasons.OrderBy(x => x.ReleaseDate).ToList(); ;
            Shows.Add(show);

        }
        IsBackgroundServiceActive = isBackgroundServiceActive;
    }

    public bool IsBackgroundServiceActive { get; set; }
    public List<ShowOutput> Shows { get; set; }
}
