﻿namespace ApplicationCore.Shows.GetShowsRecomendationsByFavorite;

public interface IGetShowsRecomendationsByFavoriteUseCase
{
    Task<GetShowsRecomendationsByFavoriteOutput> Handle(string userName, bool activateBackgroundService);
}
