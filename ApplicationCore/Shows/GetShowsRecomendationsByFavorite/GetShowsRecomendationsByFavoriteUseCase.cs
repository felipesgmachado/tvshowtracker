﻿using ApplicationCore.Shared;
using ApplicationCore.Shows.Shared;
using Domain.Favorites;
using Domain.Shows;
using Domain.Users;
using Infrastructure.Services.BackGround;
using Infrastructure.Services.Email;
using Microsoft.Extensions.DependencyInjection;
using Org.BouncyCastle.Utilities;
using System.Text;
using System.Text.Json;

namespace ApplicationCore.Shows.GetShowsRecomendationsByFavorite;

public class GetShowsRecomendationsByFavoriteUseCase : BaseUseCase, IGetShowsRecomendationsByFavoriteUseCase
{
    private readonly IShowRepository _showRepository;
    private readonly IUserRepository _userRepository;
    private readonly IFavoriteRepository _favoriteRepository;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public GetShowsRecomendationsByFavoriteUseCase(IShowRepository showRepository,
        IUserRepository userRepository, IFavoriteRepository favoriteRepository, IServiceScopeFactory serviceScopeFactory)
    {
        _showRepository = showRepository;
        _userRepository = userRepository;
        _favoriteRepository = favoriteRepository;
        _serviceScopeFactory = serviceScopeFactory;
    }

    public async Task<GetShowsRecomendationsByFavoriteOutput> Handle(string userName, bool activateBackgroundService)
    {
        var user = await _userRepository.GetByEmailAsync(userName);

        if (user == null)
            return new GetShowsRecomendationsByFavoriteOutput();

        if (!user.IsRGPDCompliance)
            return new GetShowsRecomendationsByFavoriteOutput();

        var favorites = await _favoriteRepository.GetByUserIdAsync(user.Id);

        if (favorites == null)
            return new GetShowsRecomendationsByFavoriteOutput();

        var favoriteShowsList = new List<Show>();

        foreach (var favorite in favorites)
        {
            var show = await _showRepository.GetByIdAsync(favorite.ShowId);
            favoriteShowsList.Add(show);
        }

        var recomendationList = new List<Show>();
        bool canAdd = true;

        foreach (var show in favoriteShowsList)
        {
            var showsWithSameName = (List<Show>)await _showRepository.GetWithNameAsync(show.Name);
            foreach (var item in showsWithSameName)
            {
                if (item.Id != show.Id)
                {
                    foreach (var recomendationItem in recomendationList)
                    {
                        if (recomendationItem.Id == item.Id)
                            canAdd = false;
                    }
                    if (canAdd)
                        recomendationList.Add(item);
                }
            }

            var showsWithSameGenre = (List<Show>)await _showRepository.GetByGenreAsync(show.Genre);

            foreach (var item in showsWithSameGenre)
            {
                if (item.Id != show.Id)
                {
                    foreach (var recomendationItem in recomendationList)
                    {
                        if (recomendationItem.Id == item.Id)
                            canAdd = false;
                    }
                    if (canAdd)
                        recomendationList.Add(item);
                }
            }
        }

        bool isBackgroundServiceActive = false;

        var content = ToHtmlTable(recomendationList);


        var emailAddres = new EmailAddress() { Name = user.Name, Address = user.Email };
        var subject = "Here are some recomendations from your TV Show.";
        var To = new List<EmailAddress> { emailAddres };
        var message = new Message(To, subject, content);

        using (var scope = _serviceScopeFactory.CreateScope())
        {
            var service = scope.ServiceProvider.GetRequiredService<BackgroundHostedService>();
            service.Message = message;

            if (activateBackgroundService)
            {
                service.IsEnabled = true;
                isBackgroundServiceActive = true;
            }
            else
            {
                service.IsEnabled = false;
                isBackgroundServiceActive = false;
            }
        }

        return new GetShowsRecomendationsByFavoriteOutput(recomendationList, isBackgroundServiceActive);
    }

    private string ToHtmlTable(List<Show> shows)
    {
        var html = new StringBuilder("<table>");       
        //Header
        
        html.Append("<thead><tr>");

        html.Append("<th> Name </th>");
        html.Append("<th> Genre </th>");       
        html.Append("<th> Type </th>");
        html.Append("<th> Release Date </th>");
        html.Append("<th> Seasons </th>");
        html.Append("<th> Actors </th>");
        html.Append("<th> Details </th>");

        html.Append("</tr></thead>");

        //Body
        html.Append("<tbody>");

        foreach (var show in shows)
        {
            html.Append("<tr>");

            html.Append("<td>" + show.Name + "</td>");

            html.Append("<td>" + show.Genre + "</td>");

            html.Append("<td>" + show.Type + "</td>");

            html.Append("<td>" + show.ReleaseDate.ToString("dd/MM/yyyy") + " </td>");

            if (show.Type == Domain.Shows.Type.Movies)
                html.Append("<td>" + 0 + "</td>");
            else
                html.Append("<td>" + show.Seasons.Count + "</td>");

            html.Append("<td> ");

            foreach (var item in show.Actors)
            {
                html.Append(item.Name + ", ");
            }

            html.Append("</td> ");

            html.Append("<td>" + show.Details + "</td>");

            html.Append("</tr>");
        }

        html.Append("</tbody>");
        html.Append("</table>");
        return html.ToString();
    }
}
