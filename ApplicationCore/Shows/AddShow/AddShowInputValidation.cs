﻿using FluentValidation;

namespace ApplicationCore.Shows.AddShow;

public class AddShowInputValidation : AbstractValidator<AddShowInput>
{
    public AddShowInputValidation()
    {
        RuleFor(c => c.Name).NotEmpty().WithMessage("Name is required");
        RuleFor(c => c.Details).NotEmpty().WithMessage("Details is required");
        RuleFor(c => c.ReleaseDate).NotEmpty().WithMessage("Release Date is required");
        RuleFor(c => c.Seasons.Count).GreaterThan(0).WithMessage("Seasons is required");
        //TODO: Rule for Actors
        //TODO: Rule for Episodes
        //RuleFor(c => c.Seasons.Any(x => x.Episodes.Count)).GreaterThan(0).WithMessage("Seasons is required");
    }
}
