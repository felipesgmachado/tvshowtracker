﻿using ApplicationCore.Shared;
using Domain.Actors;
using Domain.Seasons;
using Domain.Shows;

namespace ApplicationCore.Shows.AddShow;

public class AddShowOutput : BaseOutput
{
    public AddShowOutput()
    { }

    public AddShowOutput(Show entity)
    {
        IsError = false;

        Id = entity.Id;
        Name = entity.Name;
        Details = entity.Details;
        ReleaseDate = entity.ReleaseDate;        
        Genre = entity.Genre.ToString();
        Type = entity.Type.ToString();
        Actors = entity.Actors;
        Seasons = entity.Seasons;
    }

    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Details { get; set; }
    public DateTime ReleaseDate { get; set; }        
    public string Genre { get; set; }
    public string Type { get; set; }
    public List<Actor> Actors { get; set; }
    public List<Season> Seasons { get; set; }
}
