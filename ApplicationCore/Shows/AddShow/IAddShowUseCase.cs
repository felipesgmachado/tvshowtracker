﻿namespace ApplicationCore.Shows.AddShow;

public interface IAddShowUseCase
{
    Task<AddShowOutput> Handle(AddShowInput input);
}
