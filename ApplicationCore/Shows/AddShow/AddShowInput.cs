﻿using ApplicationCore.Shared;
using Domain.Actors;
using Domain.Shows;
using Type = Domain.Shows.Type;

namespace ApplicationCore.Shows.AddShow;

public class AddShowInput : BaseInput
{
    public string Name { get; set; }
    public string Details { get; set; }
    public DateTime ReleaseDate { get; set; }
    public Genre Genre { get; set; }
    public Type Type { get; set; }
    public List<Actor> Actors { get; set; }
    public List<AddShowSeasonInput> Seasons { get; set; }
}

public class AddShowSeasonInput
{
    public string Name { get; set; }
    public DateTime ReleaseDate { get; set; }
    public string Details { get; set; }
    public List<AddShowSeasonEpisodeInput> Episodes { get; set; }
}

public class AddShowSeasonEpisodeInput
{
    public int Number { get; set; }
    public string Title { get; set; }
    public DateTime ReleaseDate { get; set; }
    public string Details { get; set; }
    public int Duration { get; set; }
}
