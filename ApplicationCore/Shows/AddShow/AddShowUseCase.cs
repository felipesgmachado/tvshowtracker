﻿using ApplicationCore.Shared;
using Domain.Actors;
using Domain.Episodes;
using Domain.Seasons;
using Domain.Shows;
using Infrastructure.Services.Transations;

namespace ApplicationCore.Shows.AddShow;

public class AddShowUseCase : BaseUseCase, IAddShowUseCase
{
    private readonly IUnitOfWork _uow;
    private readonly IShowRepository _showRepository;
    private readonly IEpisodeRepository _episodeRepository;
    private readonly ISeasonRepository _seasonRepository;
    private readonly IActorRepository _actorRepository;

    public AddShowUseCase(IUnitOfWork uow, IShowRepository showRepository, IEpisodeRepository episodeRepository, ISeasonRepository seasonRepository, IActorRepository actorRepository)
    {
        _showRepository = showRepository;
        _episodeRepository = episodeRepository;
        _seasonRepository = seasonRepository;
        _actorRepository = actorRepository;
        _uow = uow;
    }

    public async Task<AddShowOutput> Handle(AddShowInput input)
    {
        if (!IsValid(new AddShowInputValidation(), input))
            return new AddShowOutput();

        //TODO: Add logger
        if (!input.Actors.Any())
            return new AddShowOutput();        

        if (!input.Seasons.Any() || !input.Seasons.Any(x => x.Episodes.Any()))
            return new AddShowOutput();        

        var newShow = new Show(input.Name, input.Details, input.ReleaseDate, input.Genre, input.Type);

        var show = await _showRepository.GetByNameAsync(input.Name);

        if (show != null)
            return new AddShowOutput();        

        foreach (var inputActor in input.Actors)
        {
            var newActor = new Actor(inputActor.Name);

            var actor = await _actorRepository.GetShowsByActorAsync(inputActor.Name);

            if (actor == null)
            {
                newShow.Actors.Add(newActor);
                newActor.Shows.Add(newShow);
                await _actorRepository.InsertAsync(newActor);
            }
            else
            {
                newShow.Actors.Add(actor);
                actor.Shows.Add(newShow);
            }                
        }

        foreach (var inputSeason in input.Seasons)
        {
            var season = new Season(newShow.Id, inputSeason.Name, inputSeason.Details, inputSeason.ReleaseDate);
            await _seasonRepository.InsertAsync(season);

            foreach (var inputEpisodes in inputSeason.Episodes)
            {
                var episode = new Episode(season.Id, inputEpisodes.Number, inputEpisodes.Title, inputEpisodes.Details, inputEpisodes.ReleaseDate, inputEpisodes.Duration);
                await _episodeRepository.InsertAsync(episode);
            }
        }

        await _uow.Commit();

        return new AddShowOutput(newShow);
    }
}
