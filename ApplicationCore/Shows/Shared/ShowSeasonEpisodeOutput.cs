﻿namespace ApplicationCore.Shows.Shared;

public class ShowSeasonEpisodeOutput
{
    public int Number { get; set; }
    public string Title { get; set; }
    public DateTime ReleaseDate { get; set; }
    public string Details { get; set; }
    public int Duration { get; set; }
}
