﻿using ApplicationCore.Shared;

namespace ApplicationCore.Shows.Shared;

public class ShowOutput : BaseInput
{
    public string Name { get; set; }
    public string Details { get; set; }
    public DateTime ReleaseDate { get; set; }
    public string Genre { get; set; }
    public string Type { get; set; }
    public List<string> Actors { get; set; }
    public List<ShowSeasonOutput> Seasons { get; set; }
}
