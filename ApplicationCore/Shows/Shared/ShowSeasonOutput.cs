﻿namespace ApplicationCore.Shows.Shared;

public class ShowSeasonOutput
{
    public string Name { get; set; }
    public DateTime ReleaseDate { get; set; }
    public string Details { get; set; }
    public List<ShowSeasonEpisodeOutput> Episodes { get; set; }
}
