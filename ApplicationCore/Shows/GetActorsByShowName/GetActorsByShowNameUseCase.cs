﻿using ApplicationCore.Shared;
using Domain.Shows;

namespace ApplicationCore.Shows.GetActorsByShowName;

public class GetActorsByShowNameUseCase : BaseUseCase, IGetActorsByShowNameUseCase
{        
    private readonly IShowRepository _showRepository;

    public GetActorsByShowNameUseCase(IShowRepository showRepository)
    {
        _showRepository = showRepository;
    }

    public async Task<GetActorsByShowNameOutput> Handle(string input)
    {
        var show = await _showRepository.GetActorsByShowAsync(input);

        if (show == null)
            return new GetActorsByShowNameOutput();

        return new GetActorsByShowNameOutput(show);
    }
}
