﻿namespace ApplicationCore.Shows.GetActorsByShowName;

public interface IGetActorsByShowNameUseCase
{
    Task<GetActorsByShowNameOutput> Handle(string input);
}
