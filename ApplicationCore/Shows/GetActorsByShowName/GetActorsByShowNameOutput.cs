﻿using ApplicationCore.Shared;
using ApplicationCore.Shows.GetAllShows;
using Domain.Shows;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Type = Domain.Shows.Type;

namespace ApplicationCore.Shows.GetActorsByShowName;

public class GetActorsByShowNameOutput : BaseOutput
{
    public GetActorsByShowNameOutput()
    {
            
    }

    public GetActorsByShowNameOutput(Show entity)
    {
        IsError = false;

        //Id = entity.Id;
        Show = entity.Name;
        //Details = entity.Details;
        //ReleaseDate = entity.ReleaseDate;
        //Genre = entity.Genre.ToString();
        //Type = entity.Type.ToString();
        Actors = entity.Actors.Select(a => a.Name).ToList();      
    }    

    //public Guid Id { get; set; }
    public string Show { get; set; }
    //public string Details { get; set; }
    //public DateTime ReleaseDate { get; set; }
    //public string Genre { get; set; }
    //public string Type { get; set; }
    public List<string> Actors { get; set; }
}
