﻿using ApplicationCore.Shared;

namespace ApplicationCore.Shows.GetAllShows;

public interface IGetAllShowsUseCase
{
    Task<(int, GetAllShowsOutput)> Handle(BaseInput input);
}
