﻿using ApplicationCore.Shared;
using Domain.Shows;
using System.Collections.Generic;

namespace ApplicationCore.Shows.GetAllShows;

public class GetAllShowsUseCase : BaseUseCase, IGetAllShowsUseCase
{
    private readonly IShowRepository _showRepository;
    public GetAllShowsUseCase(IShowRepository showRepository)
    {
        _showRepository = showRepository;
    }

    public async Task<(int, GetAllShowsOutput)> Handle(BaseInput input)
    {
        (int total, IEnumerable<Show> shows) = await _showRepository.GetAllShowsAsync(input.PageSize, input.PageIndex, input.OrderBy);

        if (shows == null)
            return (default, new GetAllShowsOutput());

        return (total, new GetAllShowsOutput(shows.ToList()));
    }
}
