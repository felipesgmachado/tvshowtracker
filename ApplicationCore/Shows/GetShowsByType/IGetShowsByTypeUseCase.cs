﻿using ApplicationCore.Shared;
using ApplicationCore.Shows.GetShowsByGenre;
using Domain.Shows;
using Type = Domain.Shows.Type;

namespace ApplicationCore.Shows.GetShowsByType
{
    public interface IGetShowsByTypeUseCase
    {
        Task<(int, GetShowsByTypeOutput)> Handle(Type type, BaseInput input);
    }
}
