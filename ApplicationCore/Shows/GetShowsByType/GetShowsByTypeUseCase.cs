﻿using ApplicationCore.Shared;
using Domain.Shows;
using Type = Domain.Shows.Type;

namespace ApplicationCore.Shows.GetShowsByType
{
    public class GetShowsByTypeUseCase : BaseUseCase, IGetShowsByTypeUseCase
    {
        private readonly IShowRepository _showRepository;

        public GetShowsByTypeUseCase(IShowRepository showRepository)
        {
            _showRepository = showRepository;
        }

        public async Task<(int, GetShowsByTypeOutput)> Handle(Type type, BaseInput input)
        {
            (int total, IEnumerable<Show> shows) = await _showRepository.GetByTypeAsync(type, input.PageSize, input.PageIndex, input.OrderBy);

            if (shows == null)
                return (default, new GetShowsByTypeOutput());

            return (total, new GetShowsByTypeOutput(shows.ToList()));
        }
    }
}
