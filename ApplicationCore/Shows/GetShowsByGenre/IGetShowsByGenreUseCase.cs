﻿using ApplicationCore.Shared;
using Domain.Shows;

namespace ApplicationCore.Shows.GetShowsByGenre
{
    public interface IGetShowsByGenreUseCase
    {
        Task<(int, GetShowsByGenreOutput)> Handle(Genre genre, BaseInput input);
    }
}
