﻿using ApplicationCore.Shared;
using Domain.Shows;

namespace ApplicationCore.Shows.GetShowsByGenre
{
    public class GetShowsByGenreUseCase : BaseUseCase, IGetShowsByGenreUseCase
    {
        private readonly IShowRepository _showRepository;

        public GetShowsByGenreUseCase(IShowRepository showRepository)
        {
            _showRepository = showRepository;
        }

        public async Task<(int, GetShowsByGenreOutput)> Handle(Genre genre, BaseInput input)
        {
            (int total, IEnumerable<Show> shows) = await _showRepository.GetByGenreAsync(genre, input.PageSize, input.PageIndex, input.OrderBy);

            if (shows == null)
                return (default, new GetShowsByGenreOutput());

            return (total, new GetShowsByGenreOutput(shows.ToList()));
        }
    }
}
