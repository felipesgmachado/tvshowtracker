﻿using System.Text.Json.Serialization;

namespace ApplicationCore.Shared;

public class BaseInput
{
    [JsonIgnore]
    public int PageSize { get; set; } = 20;

    [JsonIgnore]
    public int PageIndex { get; set; } = 1;

    [JsonIgnore]
    public string OrderBy { get; set; } = string.Empty;
}
