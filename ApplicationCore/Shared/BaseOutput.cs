﻿using System.Text.Json.Serialization;

namespace ApplicationCore.Shared;
public class BaseOutput
{    
    [JsonIgnore]
    public bool IsError { get; set; } = true;
}
