﻿using System.Text.Json.Serialization;

namespace ApplicationCore.Users.Auth.LoginUser;

public class LoginUserOutput
{
    public LoginUserOutput()
    {
        IsError = true;
    }

    public LoginUserOutput(string token)
    {
        IsError = false;

        Token = token;
    }

    [JsonIgnore]
    public bool IsError { get; set; }
    public string Token { get; set; }
}
