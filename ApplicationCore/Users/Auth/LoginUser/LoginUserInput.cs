﻿namespace ApplicationCore.Users.Auth.LoginUser;

public class LoginUserInput
{
    public string Email { get; set; }
    public string Password { get; set; }
}
