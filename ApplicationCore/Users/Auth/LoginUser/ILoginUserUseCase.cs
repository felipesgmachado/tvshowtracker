﻿namespace ApplicationCore.Users.Auth.LoginUser;

public interface ILoginUserUseCase
{
    Task<LoginUserOutput> Handle(LoginUserInput input);
}
