﻿using ApplicationCore.Shared;
using Domain.Users;
using Infrastructure.Services.Auth;

namespace ApplicationCore.Users.Auth.LoginUser;

public class LoginUserUseCase : BaseUseCase, ILoginUserUseCase
{
    private readonly IAuthService _authService;
    private readonly IUserRepository _userRepository;

    public LoginUserUseCase(IAuthService authService, IUserRepository userRepository)
    {
        _authService = authService;
        _userRepository = userRepository;
    }

    public async Task<LoginUserOutput> Handle(LoginUserInput input)
    {
        // TODO: validar o input

        // Add logger

        var token = await _authService.LoginUserAsync(input.Email, input.Password);
        var user = await _userRepository.GetByEmailAsync(input.Email);

        if (string.IsNullOrEmpty(token) || user is null)
            return new LoginUserOutput();


        return new LoginUserOutput(token);
    }
}
