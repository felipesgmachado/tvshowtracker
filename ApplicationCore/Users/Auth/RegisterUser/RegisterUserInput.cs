﻿namespace ApplicationCore.Users.Auth.RegisterUser;

public class RegisterUserInput
{
    public string Email { get; set; }
    public string Name { get; set; } = string.Empty;
    public bool IsRgpdCompliance { get; set; }
    public string Password { get; set; }
}
