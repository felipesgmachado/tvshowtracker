﻿using Domain.Users;
using System.Text.Json.Serialization;

namespace ApplicationCore.Users.Auth.RegisterUser;

public class RegisterUserOutput
{
    public RegisterUserOutput()
    {
        IsError = true;
    }

    public RegisterUserOutput(User entity)
    {
        IsError = false;

        Id = entity.Id;
        Email = entity.Email;
    }

    [JsonIgnore]
    public bool IsError { get; set; }
    public Guid Id { get; set; }
    public string Email { get; set; }
}
