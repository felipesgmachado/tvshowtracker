﻿using ApplicationCore.Shared;
using Domain.Users;
using Infrastructure.Services.Auth;

namespace ApplicationCore.Users.Auth.RegisterUser;

public class RegisterUserUseCase : BaseUseCase, IRegisterUserUseCase
{
    private readonly IAuthService _authService;
    private readonly IUserRepository _userRepository;

    public RegisterUserUseCase(IAuthService authService, IUserRepository userRepository)
    {
        _authService = authService;
        _userRepository = userRepository;
    }

    public async Task<RegisterUserOutput> Handle(RegisterUserInput input)
    {
        // TODO: validar o input

        // Add logger


        var isRegistered = await _authService.RegisterUserAsync(input.Email, input.Password);

        if (!isRegistered)
            return new RegisterUserOutput();

        var user = new User(input.Name, input.Email, input.IsRgpdCompliance);

        // Add unit of work
        await _userRepository.InsertAsync(user);

        return new RegisterUserOutput(user);
    }
}
