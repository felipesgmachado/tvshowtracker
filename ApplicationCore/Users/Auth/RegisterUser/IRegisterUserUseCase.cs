﻿namespace ApplicationCore.Users.Auth.RegisterUser;

public interface IRegisterUserUseCase
{
    Task<RegisterUserOutput> Handle(RegisterUserInput input);
}
