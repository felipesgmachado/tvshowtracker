﻿using Application.Queries.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries
{
    public  interface IUserQueryHandler
    {
        Task<UserResponse?> Handle(Guid id);
        Task <List<UserResponse>?> Handle();
    }
}
